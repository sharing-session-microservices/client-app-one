package com.sc.sharingsession.clientappone.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

@RestController
public class HelloController {

    @Value("${testing.message}")
    String message;

    @GetMapping("/hello/{message}")
    public String hello(@PathVariable String message){
        return message+", "+ Objects.toString(message, "World");
    }

}
