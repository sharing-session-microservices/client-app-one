package com.sc.sharingsession.clientappone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class ClientAppOneApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClientAppOneApplication.class, args);
	}

	@GetMapping("/")
	public String welcome(){
		return "Welcome to client app one";
	}
}
